Official TradeIo API documentations

|**Name**|**Description**|
|---|---|
|Rest-Api|**https://gitlab.com/trade-io/official-api-docs/blob/master/rest-api.md**|
|Sign|**https://gitlab.com/trade-io/official-api-docs/blob/master/sign_request.md**|
|C# client|**https://gitlab.com/trade-io/csharpclient**|
|Bash/curl client|**https://gitlab.com/trade-io/official-api-docs/blob/master/api.sh**|